$("./body") {
  add_class("mw-story-master")

  # Page removals
  remove(".//div[@id='rightcolumn']")
  remove(".//ul[@id='skiplinks']")
  remove(".//div[@id='mastad']")
  remove(".//div[@class='footer_ad' or contains(@class, 'ribbonAd')]")

  # Box Appearance
  $(".//div[@id='wrapper_sleeve']") {
    add_class("mw-box")
  }

  # Main content setup
  $(".//div[@id='content']") {
    # Removals
    remove(".//div[@class='categories']")
    remove(".//div[@class='pageOptions']")

    # Organization
    # Move the related content to the bottom of the content box
    $(".//div[@id='related_content']") {
      $("./div[@class='sleeve']") {
        add_class("mw-related-articles")
        move_to("//div[@id='relatedItems']", "top")
      }
    }

    # Add some typography classes
    $(".//div[@class='story_title']") {
      add_class("clear")
      $("./h1") {
        add_class("mw-story-title")
      }
      # Insert the share icon before the title
      insert_top("div", class: "mw-share pull-right", data-toggle: "collapse", data-target: ".mw-share-items") {
        insert_after("div", class: "mw-share-items collapse") {
          insert_bottom("div", class: "sprites-twitter")
          insert_bottom("div", class: "sprites-linkedin")
        }
      }
      move_here("../p[@class='byline']", "bottom")
      $("..") {
        add_class("clearfix")
      }
    }
    # Set up the story title byline topography
    $(".//div[@class='byline']") {
      $(".//span[@class='date']") {
        add_class("mw-date-title")
      }
      $(".//span[@class='author']") {
        add_class("mw-author-title")
      }
    }

    # Remove extraneous p tags ruining the vertical rhythm
    $(".//div[@class='maincontent_wrapper']") {
      removeEmptyTags("p")
    }

    # Set up the comments form
    $(".//div[@id='comments_form']") {
      $(".//h2") {
        add_class("mw-h2-red-comments")
      }
      # Simplify Required Information
      $(".//div[@class='alert']") {
        $("./p") {
          text("All Fields Required")
        }
      }
      # Set up the placeholder attributes
      $(".//input[@id='comments_name']") {
        attribute("placeholder", "Name")
      }
      $(".//input[@id='comments_email']") {
        attribute("placeholder", "Email")
      }
      $(".//textarea[@id='comments_comment']") {
        attribute("placeholder", "Comment")
        attribute("rows", "4")
      }
      # Set up the layout for the form
      $(".//div[@class='LoggedIn']") {
        add_class("row")
        # Fix floating clear
        $("./div[@class='field']") {
          add_class("clear")
        }
        # Text Inputs
        $(".//div[contains(concat(' ', @class, ' '), ' field ') and contains(concat(' ', @class, ' '), ' inline ')]") {
          add_class("one half")
          add_class("mw-input-half")
          add_class("mw-" + index())
        }
        # Fix Radio Buttons
        $(".//label[@for='postanonno']/..") {
          add_class("mw-post-as")
          add_class("row")
          $("./label[@class='inline-label']") {
            add_class("one fourth")
          }
        }
        # Button
        $(".//button[@id='CommentInsertBtn']") {
          add_class("mw-btn-red2")
        }
      }
    }

    # Set up items after the Comment Form
    $(".//div[@id='relatedItems']") {
      remove(".//@style")
      # Add the right class to the header
      $("./div[contains(concat(' ', @class, ' '), ' mw-related-articles ')]|.//div[@id='rss_read']") {
        add_class("mw-top-margin")
        $(".//h2") {
          add_class("mw-h2-red-header")
        }
      }
      $("./div[contains(concat(' ', @class, ' '), ' relatedbriefings ')]") {
        add_class("mw-top-margin")
        $(".//h2") {
          add_class("mw-h2-yellow-header")
        }
      }
    }
  }
}
