$("./body") {
  add_class("mw-spin2")

  # Page Removals
  remove(".//ul[@id='skiplinks']")
  remove(".//div[@id='mastad']")
  remove(".//div[@id='rightcolumn']")
  remove(".//a[@class='rssfeed']")
  $(".//div[contains(concat(' ', @class, ' '), ' column ')]") {
    remove_class("column")
  }

  # Add in classes for layout
  $(".//div[@id='colwrapper']") {
    add_class("mw-box")
    # Headings
    $(".//div[@class='sectionhead_sleeve']") {
      $("./h2") {
        add_class("mw-h2-black-header")
      }
    }
    $(".//h2/a") {
      add_class("mw-h2")
    }
    $(".//p[@class='meta']") {
      add_class("mw-date-title")
    }
  }

}
