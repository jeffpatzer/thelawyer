$("./body") {
  # Set up the footer
  $(".//div[@id='footer']") {
    # Removals
    remove("./p[@class='webvision']")
    # Organize the rows
    $(".//div[contains(concat(' ', @class, ' '), ' top ')]") {
      # First row
      $("./div[contains(concat(' ', @class, ' '), ' block ') and contains(concat(' ', @class, ' '), ' one ')]") {
        $("./div") {
          add_class("row")
          $("./a") {
            add_class("two fifths")
            insert_after("div", class: "mw-container three fifths") {
              move_here("./following-sibling::*", "bottom")
              $(".//a[contains(concat(' ', @class, ' '), ' button ')]") {
                add_class("mw-btn-black")
                $("..") {
                  add_class("mw-list-none")
                }
              }
            }
          }
        }
      }
    }
    $(".//div[contains(concat(' ', @class, ' '), ' middle ')]") {
      # Third row
      $(".//div[contains(concat(' ', @class, ' '), ' block ') and contains(concat(' ', @class, ' '), ' three ')]") {
        $("./div") {
          add_class("row")
          $(".//li") {
            add_class("one half")
            add_class("mw-third")
          }
        }
      }
    }
    $(".//div[contains(concat(' ', @class, ' '), ' bottom ')]") {
      remove("//@style")
      # Fourth row
      $(".//div[contains(concat(' ', @class, ' '), ' block ') and contains(concat(' ', @class, ' '), ' four ')]") {
        add_class("row")
        move_here("./div/div", "bottom")
        move_here("../div[contains(concat(' ', @class, ' '), ' five ')]", "bottom")
        move_here("../div[contains(concat(' ', @class, ' '), ' six ')]", "bottom")
        remove("./div[@class='htmlContent']")
        $("./div") {
          add_class("one fourth")
        }
      }
    }
  }
}
