$('./body') {
  # Remove a default class on the body
  remove_class("js-disabled")
  remove(".//div[@id='cookies_box']")
  $(".//div[@id='wrapper_sleeve']") {
    # Insert the header container
    insert_before("header", class: "mw-header clear") {
      # Logo
      insert_bottom("div", class: "mw-logo-container") {
        insert_bottom("div", class: "mw-logo center")
      }
      # Search
      move_here("//div[@id='mastsearch']", "bottom") {
        add_class("pull-right")
        $(".//input[@type='text']") {
          attribute("placeholder", "Search thelawyer.com...")
        }
        $(".//button") {
          add_class("pull-right")
          add_class("mw-btn-search")
        }
      }
      # Menu
      insert_top("div", class: "mw-menu-container pull-left") {
        insert_top("a", class: "mw-menu", href: "#mw-panel")
      }
      # Navigation Panel
      insert_before("div", data-role: "panel", id: "mw-panel", data-display: "push") {
        # Move items into the panel
        move_here("//div[@class='topSleeve']", "bottom")
        move_here("//div[@id='sleeve']", "bottom")
      }
      # Removals
      remove("//div[@id='mast']")
      remove("//div[@id='mainnav']")
      remove("//div[@id='mast_bar']")
    }
  }

  # Clean up the Panel
  $(".//div[@id='mw-panel']") {
    $(".//div[@id='topNav']") {
      # Removals
      $("./ul/li[not(@class='last')]") {
        remove()
      }
      # Moving
      $("..//div[@id='mastnav']") {
        $("./ul") {
          add_class("main")
        }
        move_to("../..", "bottom")
      }
    }
  }
}
