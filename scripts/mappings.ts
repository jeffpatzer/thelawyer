/*
  Mappings

  Mappings are matchers that we use to determine if we should execute a
  bit of Tritium during an execution. Aka, run something when we are
  are on a certain page.

  Example starting code:
*/

match($status) {

  with(/302/) {
    log("--> STATUS: 302") # redirect: just let it go through
  }

  with(/200/) {
    log("--> STATUS: 200")

    # The site uses a variety of "templates". These templates are included in the
    # id's and classes of the page. Rather than matching on the standard path, we will
    # match on those id's and classes.

    $templateID = fetch("//body/@id")
    $templateClass = fetch("//body/@class")
    log(concat("=>  $templateID:  ", $templateID))
    log(concat("=>  $templateClass:  ", $templateClass))

    match($templateID) {
      with(/story/) {
        match($templateClass) {
          with(/story-Master/i) {
            log("--> Importing pages/story_master.ts")
            @import pages/story_master.ts;
          }
        }
      }
      with(/spin/) {
        match($templateClass) {
          with(/spin2\-master/i) {
            log("--> Importing pages/spin2-master.ts")
            @import pages/spin2-master.ts;
          }
        }
      }

      else() {
        log("--> No page match in mappings.ts")
      }
    }
  }

  else() {
    # not 200 or 302 response status
    log("--> STATUS: " + $status + " assuming its an error code pages/error.ts")
    @import pages/error.ts
  }

}
